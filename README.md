### 1.A Create a new repository

    git clone https://brzezinavaclav@bitbucket.org/brzezinavaclav/dashboard-fe.git
    cd dashboard-fe

### 1.B Existing folder or Git repository

    cd existing_folder
    git init
    git remote add origin https://brzezinavaclav@bitbucket.org/brzezinavaclav/dashboard-fe.git
    git add .
    git commit
    git push -u origin master

### 2 Install dependencies

    npm install

### 3.A Run debug

    npm start

### 3.B Build project

    npm run build
