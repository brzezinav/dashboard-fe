import firebase from 'firebase';

export const base = firebase.initializeApp({
    apiKey: "AIzaSyDTGPBnu-QAY9d-HFsArYVM1YYh-NqmfK4",
    authDomain: "evolve-dashboard.firebaseapp.com",
    databaseURL: "https://evolve-dashboard.firebaseio.com",
    projectId: "evolve-dashboard",
    storageBucket: "evolve-dashboard.appspot.com",
    messagingSenderId: "401051310244"
});

export const database = base.database();