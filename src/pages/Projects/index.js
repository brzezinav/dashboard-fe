import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import Loader from '../../components/Loader/';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as projects from '../../actions/projectActions';
import Modal from '../../components/Modal';
import styles from './projects.css';

class Projects extends Component {
    constructor() {
        super();

        this.toggleModal = this.toggleModal.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.renderProjects = this.renderProjects.bind(this);
        this.addProject = this.addProject.bind(this);
        this.updateProject = this.updateProject.bind(this);
    }
    componentWillMount() {
        this.props.project_functions.getProjects();
        this.state = {}
    }
    componentWillReceiveProps(props) {
        this.setState({...props.projects});
    }
    render () {
        if(Object.keys(this.props.projects).length !== 0) {
            return (
                <div className="col-xs-12 col-md-10 wrapper" styleName="main">
                    <div className="row" styleName="table-header">
                        <div className="col-xs padding-top--thin">
                            <h3 className="no-margin">Seznam projektů Evolve</h3>
                        </div>
                        <div className="col-xs text-right">
                            <button className="btn btn-orange" onClick={this.props.project_functions.toggleModal}><span className="fa fa-plus" /> Přidat projekt</button>
                        </div>
                    </div>
                    <div styleName="table">
                        <div className="row" styleName="row header">
                            <div className="col-xs-2" styleName="cell">
                                Projekt
                            </div>
                            <div className="col-xs-2" styleName="cell">
                                PRG maintainer
                            </div>
                            <div className="col-xs-2" styleName="cell">
                                PRG zástup
                            </div>
                            <div className="col-xs-2" styleName="cell">
                                HTM maintainer
                            </div>
                            <div className="col-xs-2" styleName="cell">
                                HTM zástup
                            </div>
                            <div className="col-xs-2" styleName="cell">
                                Poznámka
                            </div>
                        </div>
                        {this.renderProjects()}
                    </div>
                    <Modal title="Přidat projekt" open={this.props.modal} closeModal={this.props.project_functions.toggleModal} body={this.renderModal()} />
                </div>
            );
        }
        else return <Loader/>;
    }
    renderProjects() {
        var projects = this.state;
/*         for (var key in this.props.projects) {
            projects.push(this.props.projects[key]);
        }
        projects.sort((a,b) => {
            if (a.name < b.name)
              return -1;
            if (a.name > b.name)
              return 1;
            return 0;
        }); */
        return Object.keys(projects).map(function (key, index) {
            return (
                <div className="row" styleName="row" key={index}>
                    <input name="name" className="col-xs-2" styleName="cell" type="text" value={projects[key].name} onChange={(e) => this.updateFieldState(e.target, key, projects[key])} onBlur={(e) => this.updateProject(e.target, key, projects[key])} onKeyUp={(e) => e.which === 13 && e.target.blur()} />
                    <input name="prgMaintain" className="col-xs-2" styleName="cell" type="text" value={projects[key].prgMaintain} onChange={(e) => this.updateFieldState(e.target, key, projects[key])} onBlur={(e) => this.updateProject(e.target, key, projects[key])} onKeyUp={(e) => e.which === 13 && e.target.blur()} />
                    <input name="prgSecond" className="col-xs-2" styleName="cell" type="text" value={projects[key].prgSecond} onChange={(e) => this.updateFieldState(e.target, key, projects[key])} onBlur={(e) => this.updateProject(e.target, key, projects[key])} onKeyUp={(e) => e.which === 13 && e.target.blur()} />
                    <input name="htmMaintain" className="col-xs-2" styleName="cell" type="text" value={projects[key].htmMaintain} onChange={(e) => this.updateFieldState(e.target, key, projects[key])} onBlur={(e) => this.updateProject(e.target, key, projects[key])} onKeyUp={(e) => e.which === 13 && e.target.blur()} />
                    <input name="htmSecond" className="col-xs-2" styleName="cell" type="text" value={projects[key].htmSecond} onChange={(e) => this.updateFieldState(e.target, key, projects[key])} onBlur={(e) => this.updateProject(e.target, key, projects[key])} onKeyUp={(e) => e.which === 13 && e.target.blur()} />
                    <input name="note" className="col-xs-2" styleName="cell" type="text" value={projects[key].note} onChange={(e) => this.updateFieldState(e.target, key, projects[key])} onBlur={(e) => this.updateProject(e.target, key, projects[key])} onKeyUp={(e) => e.which === 13 && e.target.blur()} />
                </div>
            )
        }, this);
    }
    renderModal() {
        return(
            <form>
                <div className="form-group">
                        <label>Projekt: </label>
                        <input type="text" className="form-control" ref={(input) => { this.projectInput = input; }} />
                </div>
                <div className="form-group">
                        <label>PRG maintainer: </label> 
                        <input type="text" className="form-control" ref={(input) => { this.prgMaintain = input; }} />
                </div>
                <div className="form-group">
                        <label>PRG zástup: </label> 
                        <input type="text" className="form-control" ref={(input) => { this.prgSecond = input; }} />
                </div>
                <div className="form-group">
                        <label>HTM maintainer: </label> 
                        <input type="text" className="form-control" ref={(input) => { this.htmMaintain = input; }} />
                </div>
                <div className="form-group">
                        <label>HTM zástup: </label>
                        <input type="text" className="form-control" ref={(input) => { this.htmSecond = input; }} />
                </div>
                <div className="form-group">
                        <label>Poznámka: </label>
                        <input type="text" className="form-control" ref={(input) => { this.note = input; }} />
                </div>
                <div className="row margin-top">
                    <div className="col-xs">
                        <button className="btn btn-orange" onClick={this.addProject}><span className="fa fa-plus margin-right" />Přidat projekt</button>
                    </div>
                </div>
            </form>
        )
    }
    toggleModal(edit = {}) {
        this.projectInput.value = "";
        this.prgMaintain.value = "";
        this.prgSecond.value = "";
        this.htmMaintain.value = "";
        this.htmSecond.value = "";
        this.note.value = "";
        this.props.project_functions.toggleModal(edit);
    }
    addProject() {
        this.props.project_functions.addProject({name: this.projectInput.value, prgMaintain: this.prgMaintain.value, prgSecond: this.prgSecond.value, htmMaintain: this.htmMaintain.value, htmSecond: this.htmSecond.value, note: this.note.value});
        this.toggleModal();
    }
    updateFieldState(field, id, project) {
        let pr = {};
        pr[id] = {...project};
        pr[id][field.name] = field.value;
        this.setState({...this.state, ...pr});
    }
    updateProject(field, id, project) {
        let pr = {};
        pr[id] = {...project};
        pr[id][field.name] = field.value;
        this.props.project_functions.editProject(pr);
    }
}

function mapStateToProps(store) {
  return { projects: store.projects.data, modal: store.projects.modal}
}
function mapDispatchToProps(dispatch) {
  return {project_functions: bindActionCreators(projects, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Projects, styles, {allowMultiple: true}));