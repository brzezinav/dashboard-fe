import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import Loader from '../../components/Loader/';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as attendance from '../../actions/attendanceActions';
import * as user from '../../actions/userActions';
import AttendanceTable from '../../components/AttendanceTable/';

import styles from './attendance.css';

class Attendance extends Component {
  componentWillMount() {
    this.props.attendance_functions.getAttendance();
    this.props.user_functions.getUsers();
  }
  render() {
    if(this.props.attendance.loaded && Object.keys(this.props.users).length !== 0) {
      return (
        <div className="col-xs-12 col-md-10 wrapper" styleName="main">
          <AttendanceTable />
        </div>
      )
    }
    else return <Loader/>
  }
}

function mapStateToProps(store) {
  return { users: store.user.users, attendance: store.attendance }
}
function mapDispatchToProps(dispatch) {
  return { user_functions: bindActionCreators(user, dispatch), attendance_functions: bindActionCreators(attendance, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Attendance, styles, {allowMultiple: true}));