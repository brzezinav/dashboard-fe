import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import Loader from '../../components/Loader/';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as user from '../../actions/userActions';
import * as issues from '../../actions/issueActions';

import Board from '../../components/Board/';

import styles from './tasks.css';


const states = [
  {
    id: ["3", "10405"],
    name: 'In progress/Testing'
  },
  {
    id: ["1007", "1"],
    name: 'Open/To do'
  },
  {
    id: ["10205", "10001"],
    name: 'Waiting/Acceptation'
  }
]

const employees = [
  {
    name: 'John Smith',
    username: 'john.smith',
    shortcut: 'JSm',
    events: []
  },
  {
    name: 'John Smith',
    username: 'john.smith',
    shortcut: 'JSm',
    events: []
  },
  {
    name: 'John Smith',
    username: 'john.smith',
    shortcut: 'JSm',
    events: []
  },
  {
    name: 'John Smith',
    username: 'john.smith',
    shortcut: 'JSm',
    events: []
  },
  {
    name: 'John Smith',
    username: 'john.smith',
    shortcut: 'JSm',
    events: []
  }
];

class Tasks extends Component {
  componentWillMount() {
    let users = employees.map((user) => {
      return user.username;
    });
    this.props.issue_functions.getIssues(users, Object.keys(this.props.issues.data).length);
  }
  componentWillUnmount() {
    window.clearTimeout(window.getIssues); 
  }

  render() {
    if(this.props.issues.loaded) {
      return(<div className="col-xs-12 col-md-10 wrapper" styleName="main">
        {states.map((board, index) => {
          return (
            <Board name={board.name} status={board.id} employees={employees} key={'board_'+index} />
          )
        })}
      </div>)
    }
    else return (
      <Loader/>
    );
  }
}

function mapStateToProps(store) {
  return { user: store.user.user, issues: store.issues }
}
function mapDispatchToProps(dispatch) {
  return { issue_functions: bindActionCreators(issues, dispatch), user_functions: bindActionCreators(user, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Tasks, styles));