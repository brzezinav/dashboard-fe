import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import Loader from '../../components/Loader/';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as attendance from '../../actions/attendanceActions';
import * as issues from '../../actions/issueActions';
import AttendanceCalendar from '../../components/AttendanceCalendar/';
import SummaryBoard from '../../components/SummaryBoard/';
import CustomScroll from 'react-custom-scroll';

import styles from './dashboard.css';

const states = [
  {
    id: ["3", "10405"],
    name: 'In progress/Testing'
  },
  {
    id: ["1007", "1"],
    name: 'Open/To do'
  },
  {
    id: ["10205", "10001"],
    name: 'Waiting/Acceptation'
  }
]

class Dashboard extends Component {
  componentWillMount() {
    this.props.attendance_functions.getAttendance();
    this.props.issue_functions.getIssues([this.props.user.name], Object.keys(this.props.issues).length);
  }
  componentWillUnmount() {
    window.clearTimeout(window.getIssues); 
  }
  render() {
    if(this.props.loaded) {
      return (
        <div className="col-xs-12 col-md-10 wrapper" styleName="main">
          {this.props.user.time === "part" && 
            <div styleName="calendar-wrapper">
              <div className="col-xs-12">
                  <AttendanceCalendar events={this.getEvents(this.props.attendance)} />
              </div>
            </div>
          }
          <div className="row" styleName="summary-wrapper">
            {states.map((board, index) => {
              return (
                <SummaryBoard name={board.name} status={board.id} user={this.props.user.name} key={'board_'+index} full={this.props.user.time !== "part"} />
              )
            })}
          </div>
        </div>
      )
    }
    else return <Loader/>
  }
  getEvents(attendance) {
    return Object.keys(attendance).filter((key) => {
      return attendance[key].uid === this.props.user.uid;
    }).map((key) => {
      return attendance[key].description == "" ? {...attendance[key], id: key, description: this.props.user.position} : {...attendance[key], id: key};
    });
  }
}

function mapStateToProps(store) {
  return { user: store.user.user, attendance: store.attendance.data, issues: store.issues.data, loaded: store.attendance.loaded && store.issues.loaded}
}
function mapDispatchToProps(dispatch) {
  return { issue_functions: bindActionCreators(issues, dispatch), attendance_functions: bindActionCreators(attendance, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Dashboard, styles, {allowMultiple: true}));