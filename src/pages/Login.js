import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import styles from '../styles/login.css';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as user from '../actions/userActions';
import {Redirect} from 'react-router-dom';

import Loader from '../components/Loader/';

class Login extends Component {

  constructor() {
      super();
      this.authenticateUser = this.authenticateUser.bind(this);
      this.authError = "";
  }

  componentWillMount() {
    this.props.user_functions.setUser();
  }
  
  componentWillReceiveProps(props) {
      if(props.user.authenticated) this.props.user_functions.setUser();
  }

  authenticateUser(e) {
        e.preventDefault();
        this.props.user_functions.authUser(this.email.value, this.password.value);
  }
    
  render() {
    if(this.props.user.logged !== undefined) {
        if (this.props.user.logged) {
            if(this.props.user.first_login == 1) {
                return <Redirect to="/account/"/>
            }
            else {
                return <Redirect to="/dashboard/"/>
            }
        }
        else {
            return (
            <div className="container-fluid wrapper" styleName="app">
                <div className="row wrapper">
                    <div className="col-md-8 hidden-xs hidden-sm" styleName="background-login">
                        <div styleName="caption">
                            <h2 styleName="caption-title"><i className="fa fa-cloud text-orange"></i> Přihlaš se do naší aplikace!</h2>
                            <p>Tento portál byl vytvořen aby nám usnadnil práci, doufám, že se Vám bude líbit.</p>
                        </div>
                    </div>
                    <div className="col-xs-12 col-md-4" styleName="login-form">
                        <h1><span className="text-orange">&amp;</span> Evolve dashboard</h1>
                        <small>K přihlášení prosím použij svůj Jira účet, přihlášení přes google se chystá</small>
                        {this.props.user.auth_error === 'wrong username/password' && <div className="text-danger margin-top--large"><strong>Špatné jméno, nebo heslo</strong></div>}
                        <form onSubmit={(e) => this.authenticateUser(e)} className="margin-top--large">
                            <div className="form-group">
                                <label htmlFor="login"><i className="fa fa-user"></i> Email</label>
                                <input type="text" ref={(input) => { this.email = input; }} className={"form-control" + this.authError} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password"><i className="fa fa-unlock"></i> Heslo</label>
                                <input type="password" ref={(input) => { this.password = input; }} className={"form-control" + this.authError} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="remember"><input id="remember" type="checkbox" ref={(input) => { this.remember = input; }} /> Tohle zatím nefunguje</label>
                            </div>
                            <div className="form-group">
                                <button type="submit" disabled={this.props.user.authentication === true} className="btn btn-orange btn-block btn-lg">Přihlásit se <i className="fa fa-sign-in"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            )
        }
    }
    else {
        return (
            <Loader/>
        )
    }
  }
}

function mapStateToProps(store) {
  return { user: store.user.user }
}
function mapDispatchToProps(dispatch) {
  return {user_functions: bindActionCreators(user, dispatch)}
}
export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Login, styles, {allowMultiple: true}));