import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as attendance from '../../actions/attendanceActions';
import * as user from '../../actions/userActions';
import Alert from '../../components/Alert';

import styles from './account.css';

class Account extends Component {
  constructor() {
      super();
      this.updateSettings = this.updateSettings.bind(this);
  }
  render() {
      return (
        <div className="col-xs-12 col-md-10 wrapper" styleName="main">
          <div className="row">
            <div className="col-xs-12" styleName="account-wrapper">
                <h1>Můj účet</h1>
                <form onSubmit={(e) => this.updateSettings(e)}>
                <div className="form-group">
                  <label>Úvazek</label>
                  <select defaultValue={this.props.user.time} className="form-control" styleName="input" ref={(input) => { this.time = input; }}>
                      <option value={null}>–</option>
                      <option value="full">Plný</option>
                      <option value="part">Částečný</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Role</label>
                  <select defaultValue={this.props.user.position} className="form-control" styleName="input" ref={(input) => { this.position = input; }}>
                      <option value={null}>–</option>
                      <option value="Front-end developer">Front-end developer</option>
                      <option value="Java developer">Java developer</option>
                      <option value="PHP developer">PHP developer</option>
                      <option value="Content Editor">Content Editor</option>
                      <option value="First-line support">First-line support</option>
                      <option value="Project manager">Project manager</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Pohlaví</label>
                  <select defaultValue={this.props.user.gender} className="form-control" styleName="input" ref={(input) => { this.gender = input; }}>
                    <option value={null}>-</option>
                    <option value={0}>Muž</option>
                    <option value={1}>Žena</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Zkratka</label>
                  <input defaultValue={this.props.user.shortcut} className="form-control" styleName="input" ref={(input) => { this.shortcut = input; }}/>
                </div>
                <div>
                    <button disabled={this.props.user.updating === true} className="btn btn-orange"><span className="fa fa-save margin-right" />Uložit</button>
                </div>
                </form>
            </div>
          </div>
          {this.props.user.updating === false && this.props.user.updated !== undefined && 
            <Alert type={this.props.user.updated ? "success" : "danger"} onClose={this.props.user_functions.closeAlert}>
            {this.props.user.updated ? "Nastavení bylo uloženo" : "Nastala chyba při ukládání"}
            </Alert>
          }
        </div>
      )
  }
  updateSettings(e) {
    e.preventDefault();
    this.props.user_functions.updateSettings({...this.props.user, time: this.time.value, position: this.position.value, shortcut: this.shortcut.value, gender: this.gender.value});
  }
}

function mapStateToProps(store) {
  return { user: store.user.user }
}
function mapDispatchToProps(dispatch) {
  return { user_functions: bindActionCreators(user, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Account, styles, {allowMultiple: true}));