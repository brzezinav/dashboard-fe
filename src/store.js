import {applyMiddleware, createStore} from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import io from 'socket.io-client';
import socketIO from 'socket.io-redux';
import reducer from './reducers/';

var middleware = [socketIO(socket), thunk];
if(process.env.NODE_ENV !== 'production') middleware.push(createLogger());

export const socket = io.connect(process.env.API_URL);
export default createStore(
  reducer,
  applyMiddleware(...middleware)
);