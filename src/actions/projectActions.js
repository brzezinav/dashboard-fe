import axios from 'axios';
import { socket } from '../store';

export function toggleModal(edit) {
    return (dispatch, getState) => {
        dispatch({ type: 'PROJECT_MODAL', edit: edit });
    }
}
export function getProjects() {
    return (dispatch, getState) => {
        dispatch({ type: 'GET_PROJECTS_PENDING' });

        axios.get(`${process.env.API_URL}/base/get/projects/`, { withCredentials: true })
            .then((response) => {
                dispatch({
                    type: "GET_PROJECTS_FULFILLED",
                    payload: { ...response.data }
                })
            })
        socket.on('insert:projects', function (project) {
            dispatch({
                type: 'ADD_PROJECT_FULFILLED',
                payload: project
            })
        });
        socket.on('update:projects', function (project) {
            dispatch({
                type: 'EDIT_PROJECT_FULFILLED',
                payload: project
            })
        });
        socket.on('delete:projects', function (id) {
            dispatch({
                type: 'REMOVE_PROJECT_FULFILLED',
                payload: id
            })
        });
    }
}
export function addProject(project) {
    return (dispatch, getState) => {
        dispatch({
            type: 'ADD_PROJECT_PENDING'
        });
        axios.get(`${process.env.API_URL}/base/set/projects/`, {
            params: {
                data: project
            },
            withCredentials: true
        })
            .then((response) => {
                let data = {};
                data[response.data.id] = { ...project }
                dispatch({
                    type: "ADD_PROJECT_FULFILLED",
                    table: 'projects',
                    payload: data,
                    meta: {
                        socket: {
                            channel: 'insert'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'ADD_PROJECT_REJECTED'
                });
            });
    }
}
export function editProject(project) {
    return (dispatch, getState) => {
        dispatch({
            type: 'EDIT_PROJECT_PENDING'
        });
        axios.get(`${process.env.API_URL}/base/update/projects/${Object.keys(project)[0]}/`, {
            params: {
                data: { ...project[Object.keys(project)[0]] }
            },
            withCredentials: true
        })
            .then((response) => {
                dispatch({
                    type: "EDIT_PROJECT_FULFILLED",
                    table: 'projects',
                    payload: project,
                    meta: {
                        socket: {
                            channel: 'update'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'EDIT_PROJECT_REJECTED'
                });
            });
    }
}
export function removeProject(id) {
    return (dispatch, getState) => {
        dispatch({
            type: 'REMOVE_PROJECT_PENDING'
        });

        axios.get(`${process.env.API_URL}/base/remove/projects/${id}/`, { withCredentials: true })
            .then((response) => {
                dispatch({
                    type: "REMOVE_PROJECT_FULFILLED",
                    table: 'projects',
                    id: id,
                    meta: {
                        socket: {
                            channel: 'delete'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'REMOVE_PROJECT_REJECTED'
                });
            });
    }
}