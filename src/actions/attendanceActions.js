import axios from 'axios';
import { socket } from '../store';

export function toggleModal(selectedDays = [], edit = {}) {
    return (dispatch, getState) => {
        dispatch({ type: 'TOGGLE_MODAL', selectedDays: selectedDays, edit: edit });
    }
}
export function setInterval(when, value) {
    return (dispatch) => {
        dispatch({ type: 'SET_INTERVAL', when: when, value: value });
    }
}
export function selectDays(selectedDays) {
    return (dispatch) => {
        dispatch({ type: 'SELECT_DAYS', payload: selectedDays });
    }
}
export function getAttendance() {
    return (dispatch, getState) => {
        dispatch({ type: 'GET_ATTENDANCE_PENDING' });
        axios.get(`${process.env.API_URL}/base/get/attendance/`, { withCredentials: true })
            .then((response) => {
                dispatch({
                    type: "GET_ATTENDANCE_FULFILLED",
                    payload: { ...response.data }
                })
            })
        socket.on('insert:attendance', function (attendance) {
            dispatch({
                type: 'ADD_ATTENDANCE_FULFILLED',
                payload: attendance
            })
        });
        socket.on('update:attendance', function (attendance) {
            dispatch({
                type: 'EDIT_ATTENDANCE_FULFILLED',
                payload: attendance
            })
        });
        socket.on('delete:attendance', function (id) {
            dispatch({
                type: 'REMOVE_ATTENDANCE_FULFILLED',
                id: id
            })
        });
    }
}
export function addAttendance(attendance) {
    return (dispatch, getState) => {
        dispatch({ type: 'ADD_ATTENDANCE_PENDING' });
        axios.get(`${process.env.API_URL}/base/set/attendance/`, {
            params: {
                data: attendance
            },
            withCredentials: true
        })
            .then((response) => {
                let data = {};
                data[response.data.id] = { ...attendance }
                dispatch({
                    type: "ADD_ATTENDANCE_FULFILLED",
                    table: 'attendance',
                    payload: data,
                    meta: {
                        socket: {
                            channel: 'insert'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'ADD_ATTENDANCE_REJECTED'
                });
            });
    }
}
export function editAttendance(id, attendance) {
    console.info(id, attendance);
    return (dispatch, getState) => {
        dispatch({ type: 'EDIT_ATTENDANCE_PENDING' });
        axios.get(`${process.env.API_URL}/base/update/attendance/${id}/`, {
            params: {
                data: attendance
            },
            withCredentials: true
        })
            .then((response) => {
                let obj = {};
                obj[id] = { ...attendance }
                dispatch({
                    type: "EDIT_ATTENDANCE_FULFILLED",
                    table: 'attendance',
                    payload: obj,
                    meta: {
                        socket: {
                            channel: 'update'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'EDIT_ATTENDANCE_REJECTED'
                });
            });
    }
}
export function removeAttendance(id) {
    return (dispatch, getState) => {
        dispatch({ type: 'REMOVE_ATTENDANCE_PENDING' });

        axios.get(`${process.env.API_URL}/base/remove/attendance/${id}/`, { withCredentials: true })
            .then((response) => {
                dispatch({
                    type: "REMOVE_ATTENDANCE_FULFILLED",
                    table: 'attendance',
                    id: id,
                    meta: {
                        socket: {
                            channel: 'delete'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'REMOVE_ATTENDANCE_REJECTED'
                });
            });
    }
}