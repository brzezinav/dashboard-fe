import axios from 'axios';

export function getIssues(users, loaded) {
    return (dispatch) => {
        var stored = loaded;
        (function doSomething() {
            if (!document.hidden) {
                if (Object.keys(users).length > stored) dispatch({ type: 'GET_ISSUES_PENDING' });
                var issues = {}
                users.map((user, index) => {
                    axios.get(`${process.env.API_URL}/jira/issues/${user}/`, { withCredentials: true })
                        .then((response) => {
                            issues[user] = response.data.issues;
                            if (Object.keys(issues).length === users.length) {
                                dispatch({
                                    type: 'GET_ISSUES_FULFILLED',
                                    data: issues
                                })
                            }
                        })
                });
                stored = Object.keys(users).length;
            }
            window.getIssues = setTimeout(doSomething, 10000);
        })();
    }
}
export function getTransitions(issue) {
    return (dispatch) => {
        dispatch({ type: 'GET_TRANSITIONS_PENDING' });
        axios.get('${process.env.API_URL}/transitions/' + issue, { withCredentials: true })
            .then(function (response) {
                dispatch({
                    type: 'GET_TRANSITIONS_FULFILLED',
                    data: response.data,
                    issue: issue
                })
            })
            .catch(function (error) {
                dispatch({
                    type: 'GET_TRANSITIONS_REJECTED',
                })
            });
    }
}