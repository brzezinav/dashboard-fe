import axios from 'axios';
import { socket } from '../store';
import { getCookie } from '../helpers';

export function closeAlert() {
    return (dispatch, getState) => {
        dispatch({
            type: "CLOSE_ALERT"
        })
    }
}
export function setUser() {
    return (dispatch, getState) => {
        axios.get(`${process.env.API_URL}/base/myself/`, { withCredentials: true }).then((response) => {
            if (response.data.error == undefined && getCookie('connect.sid') !== undefined) {
                dispatch({
                    type: "SET_USER",
                    payload: { ...response.data[Object.keys(response.data)[0]], uid: Object.keys(response.data)[0], logged: true }
                })
            }
            else {
                dispatch({
                    type: "SET_USER",
                    payload: {
                        logged: false
                    }
                })
                socket.removeAllListeners();
            }
        });
        /*         socket.on('insert:users', function(user){
                    dispatch ({
                        type: 'ADD_USER_FULFILLED',
                        payload: user
                    })
                }); */
        socket.on('update:users', function (user) {
            dispatch({
                type: 'EDIT_USER_FULFILLED',
                payload: user
            })
        });
        /*         socket.on('delete:users', function(id){
                    dispatch ({
                        type: 'REMOVE_USER_FULFILLED',
                        payload: id
                    })
                }); */
    }
}
export function firstLogin(uid) {
    return (dispatch, getState) => {
        axios.get(`${process.env.API_URL}/base/update/users/${uid}/`, {
            params: {
                data: {
                    first_login: 0
                }
            },
            withCredentials: true
        }).then(() => {
            dispatch({ type: 'FIRST_LOGIN_CLOSED' });
        })
    }
}
export function updateSettings(user) {
    return (dispatch, getState) => {
        dispatch({ type: 'UPDATE_SETTINGS_PENDING' });

        axios.get(`${process.env.API_URL}/base/update/users/${user.uid}/`, {
            params: {
                data: {
                    position: user.position,
                    time: user.time,
                    shortcut: user.shortcut,
                    gender: user.gender
                }
            },
            withCredentials: true
        })
            .then((response) => {
                dispatch({
                    type: "UPDATE_SETTINGS_FULFILLED",
                    payload: {
                        position: user.position,
                        time: user.time,
                        shortcut: user.shortcut,
                        gender: user.gender
                    },
                    table: 'users',
                    meta: {
                        socket: {
                            channel: 'update'
                        },
                    }
                })
            })
            .catch(() => {
                dispatch({
                    type: 'UPDATE_SETTINGS_REJECTED'
                });
            });
    }
}
export function authUser(username, password) {
    var token = window.btoa(`${username}:${password}`);
    return (dispatch, getState) => {
        dispatch({ type: 'AUTH_USER_PENDING' });

        axios.get(`${process.env.API_URL}/jira/auth/`, {
            params: {
                token: token
            },
            withCredentials: true
        })
            .then((response) => {
                if (response.data.error === undefined) {
                    dispatch({
                        type: "AUTH_USER_FULFILLED"
                    })
                }
                else {
                    dispatch({
                        type: "AUTH_USER_REJECTED",
                        payload: response.data.error
                    })
                }
            })
    }
}
export function getUsers() {
    return (dispatch, getState) => {
        dispatch({ type: 'GET_USERS_PENDING' });
        axios.get(`${process.env.API_URL}/base/get/users/`, { withCredentials: true })
            .then((response) => {
                dispatch({
                    type: "GET_USERS_FULFILLED",
                    payload: { ...response.data }
                })
            })
    }
}
export function logout() {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOGOUT_USER_PENDING'
        });
        axios.get(`${process.env.API_URL}/jira/logout/`, { withCredentials: true }).then(() => {
            dispatch({
                type: 'LOGOUT_USER_FULFILLED'
            });
        })
    }
}
export function updateAlert() {
    return (dispatch, getState) => {
        dispatch({
            type: 'UPDATE_ALERT'
        });
    }
}