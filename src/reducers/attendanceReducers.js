import moment from 'moment';

export default function reducer (state = {
    data: {},
    modal: false,
    calendar: {
        scrollToTime: moment(),
        selectedDays: [],
        interval: {
            start: moment().set({'hour': 9, 'minute': 0, 'second': 0}),
            end: moment().set({'hour': 18, 'minute': 0, 'second': 0})
        },
        edit: {}
    },
    loaded: false
}, action) {
    switch(action.type) {
        case "GET_ATTENDANCE_FULFILLED": {
            return {...state, data: {...state.data, ...action.payload}, loaded: true}
        }
        case "EDIT_ATTENDANCE_FULFILLED":
        case "ADD_ATTENDANCE_FULFILLED": {
            return {...state, data: {...state.data, ...action.payload}}
        }
        case "REMOVE_ATTENDANCE_FULFILLED": {
            let data = {...state.data};
            delete data[action.id];
            return {...state, data: {...data}}
        }
        case "SELECT_DAYS": {
            return {
                ...state,
                calendar: {
                    scrollToTime: state.calendar.scrollToTime,
                    selectedDays: action.payload,
                    interval: state.calendar.interval,
                    edit: state.calendar.edit
                }
            }
        }
        case "TOGGLE_MODAL": {
            return {
                ...state,
                modal: !state.modal,
                calendar: {
                    scrollToTime: state.calendar.scrollToTime,
                    selectedDays: action.selectedDays,
                    edit: action.edit,
                    interval: {
                        start: (action.edit.start && moment(action.edit.start)) || moment().set({'hour': 9, 'minute': 0, 'second': 0}),
                        end: (action.edit.end && moment(action.edit.end)) || moment().set({'hour': 18, 'minute': 0, 'second': 0}),
                    }
                }
            }
        }
        case "SET_INTERVAL": {
            var interval = {...state.calendar.interval}
            interval[action.when] = action.value;
            return {
                ...state,
                calendar: {
                    scrollToTime: state.calendar.scrollToTime,
                    selectedDays: state.calendar.selectedDays,
                    interval: interval,
                    edit: state.calendar.edit
                }
            }
        }
        default: {
            return state;
        }
    }
}