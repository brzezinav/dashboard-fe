import {combineReducers} from 'redux';
import issues from './issueReducers';
import user from './userReducers';
import attendance from './attendanceReducers';
import projects from './projectsReducers';

export default combineReducers({
    issues,
    user,
    attendance,
    projects
});