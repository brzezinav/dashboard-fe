export default function reducer (state = {
    user: {authenticated: false},
    users: {}
}, action) {
    switch(action.type) {
        case "SET_USER": {
            return {...state, user: {...state.user, ...action.payload}}
        }
        case "LOGOUT_USER_FULFILLED": {
            return {users: {}, user: {authenticated: false, logged: false}}
        }
        case "GET_USERS_FULFILLED": {
            return {...state, users: {...state.users, ...action.payload}}
        }
        case "AUTH_USER_PENDING": {
            return {...state, user: {...state.user, authentication: true, auth_error: ''}}
        }
        case "AUTH_USER_FULFILLED": {
            return {...state, user: {...state.user, authenticated: true, authentication: false}}
        }
        case "AUTH_USER_REJECTED": {
            return {...state, user: {...state.user, authenticated: false, authentication: false, auth_error: action.payload}}
        }
        case "UPDATE_SETTINGS_PENDING": {
            return {...state, user: {...state.user, updating: true, updated: false}}
        }
        case "UPDATE_SETTINGS_FULFILLED": {
            return {...state, user: {...state.user, ...action.payload, updating: false, updated: true}}
        }
        case "UPDATE_SETTINGS_REJECTED": {
            return {...state, user: {...state.user, updating: false, updated: false}}
        }
        case "CLOSE_ALERT": {
            return {...state, user: {...state.user, updated: undefined}}
        }
        default: {
            return {...state};
        }
    }
}