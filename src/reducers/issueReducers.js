export default function reducer (state = {data: {}, transitions: {}, loaded: true}, action) {
    switch(action.type) {
        case "GET_ISSUES_PENDING": {
            return {...state, loaded: false};
        }
        case "GET_ISSUES_FULFILLED": {
            return {...state, data: {...state.data, ...action.data}, loaded: true};
        }
        case "GET_ISSUES_REJECTED": {
            return {...state};
        }
        case "GET_TRANSITIONS_FULFILLED": {
            var transitions = {...state.transitions};
            transitions[action.issue] = [...action.data];
            return {...state, transitions: transitions};
        }
        case "GET_TRANSITIONS_PENDING": {
            return {...state};
        }
        case "GET_TRANSITIONS_REJECTED": {
            return {...state};
        }
        default: {
            return {...state};
        }
    }
}