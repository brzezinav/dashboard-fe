export default function reducer (state = {
    data: {},
    modal: false,
    edit: {}
}, action) {
    switch(action.type) {
        case "GET_PROJECTS_FULFILLED":
        case "ADD_PROJECT_FULFILLED":
        case "EDIT_PROJECT_FULFILLED":{
            return {...state, data: {...state.data, ...action.payload}}
        }
        case "REMOVE_PROJECT_FULFILLED": {
            let data = {...state.data};
            delete data[action.id];
            return {...state, data: {...data}}
        }
        case "PROJECT_MODAL": {
            return {
                ...state,
                modal: !state.modal,
                edit: action.edit
            }
        }
        default: {
            return state;
        }
    }
}