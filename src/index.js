import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter as Router, Route as DefaultRoute, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';

import App from './components/App/';
import Tasks from './pages/Tasks/';
import Dashboard from './pages/Dashboard/';
import Account from './pages/Account/';
import Attendance from './pages/Attendance/';
import Projects from './pages/Projects/';
import NotFound from './pages/NotFound';
import Login from './pages/Login';

import './styles/global.css';

const AppRoute = ({ component: Component, ...rest }) => (
  <DefaultRoute {...rest} render={props => (
    <App>
      <Component {...props}/>
    </App>
  )}/>
)

render(
  (<Provider store={store}>
      <Router>
        <Switch>
          <DefaultRoute exact path="/" component={Login} />
          <AppRoute path="/dashboard/" component={Dashboard} />
          <AppRoute path="/account/" component={Account} />
          <AppRoute path="/tasks/" component={Tasks} />
          <AppRoute path="/tasks/:id" component={Tasks} />
          <AppRoute path="/attendance/" component={Attendance} />
          <AppRoute path="/attendance/:id" component={Attendance} />
          <AppRoute path="/projects/" component={Projects} />
          <DefaultRoute component={NotFound} />
        </Switch>
    </Router>
  </Provider>), document.getElementById('root')
);