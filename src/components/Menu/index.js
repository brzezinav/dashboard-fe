import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import {NavLink as Link} from 'react-router-dom';

import styles from './menu.css';


class Menu extends Component {
  render() {
    return (
        <ul styleName="nav">
              {this.props.nav.map((link, i) => {
                return <li key={i}><Link to={link.path} activeClassName="active" exact={link.exact}>{link.name}</Link></li>
              })}
        </ul>    
    );
  }
}

export default CSSModules(Menu, styles, {allowMultiple: true});