import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as issues from '../../actions/issueActions';
import Issue from '../Issue/';
import { Scrollbars } from 'react-custom-scrollbars';

import styles from './board.css';


class SummaryBoard extends Component {
  constructor() {
    super();
    this.renderIssue = this.renderIssue.bind(this);
  }
  render() {
      return (
        <div className="col-xs" styleName={"board board_"+this.props.status[0]} id={"board_" + this.props.status[0]}>
            <div styleName="user_box" className="col-xs-12 col-sm">
                <div className="row">
                <div className="col-xs-12" styleName="user_box_heading">
                    {this.props.name}
                </div>
                <div className="col-xs-12" styleName={this.props.full ? "issues full" : "issues"}>
                <Scrollbars style={{ height: this.props.full ? "calc(100vh - 135px)" : 500 }}>
                {
                    this.props.issues.data[this.props.user] !== undefined &&
                    this.props.issues.data[this.props.user].filter((issue) => {
                        return this.props.status.indexOf(issue.fields.status.id) !== -1 && issue.fields.assignee.key == this.props.user;
                    }).map(this.renderIssue)
                }
                </Scrollbars>
                </div>
                </div>
            </div>  
        </div>
      );
  }
  renderIssue(issue, index) {
    issues.getTransitions(issue.key);
    return (
      <Issue key={'issue_'+index} issue={issue} />
    )
  }
}

function mapStateToProps(store) {
  return { issues: store.issues }
}
function mapDispatchToProps(dispatch) {
  return { issue_functions: bindActionCreators(issues, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(SummaryBoard, styles, {allowMultiple: true}));