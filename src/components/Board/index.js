import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as issues from '../../actions/issueActions';
import Issue from '../Issue/';
import { Scrollbars } from 'react-custom-scrollbars';

import styles from './board.css';


class Board extends Component {
  constructor() {
    super();
    this.renderIssue = this.renderIssue.bind(this);
  }
  render() {
      return (
        <div styleName={"board board_"+this.props.status[0]} id={"board_" + this.props.status[0]} data-title={this.props.name}>
          {Object.keys(this.props.issues.data).length !== 0 && this.props.employees.map(this.getIssues, {status: this.props.status, issues: this.props.issues.data, renderIssue: this.renderIssue})}     
        </div>
      );
  }
  getIssues(user, index) {
    return (
      <div styleName="user_box" className="col-xs-12 col-sm" key={'user_'+index}>
        <div className="row">
        <div className="col-xs-12" styleName="user_box_heading">
          {user.name}
        </div>
        <div className="col-xs-12" styleName="issues">
          <Scrollbars style={{ height: 400 }}>
          {
            this.issues[user.username] !== undefined &&
              this.issues[user.username].filter((issue) => {
                return this.status.indexOf(issue.fields.status.id) !== -1 && issue.fields.assignee.key == user.username;
              }).map(this.renderIssue)
          }
          </Scrollbars>
        </div>
        </div>
      </div>
  )
  }
  renderIssue(issue, index) {
    issues.getTransitions(issue.key);
    return (
      <Issue key={'issue_'+index} issue={issue} />
    )
  }
}

function mapStateToProps(store) {
  return { issues: store.issues }
}
function mapDispatchToProps(dispatch) {
  return { issue_functions: bindActionCreators(issues, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(Board, styles, {allowMultiple: true}));