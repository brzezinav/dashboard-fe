import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import Modal from './modal';

import styles from './issue.css';


class Issue extends Component {
  constructor() {
      super();
      this.closeModal = this.closeModal.bind(this);
      this.openModal = this.openModal.bind(this);
      this.state = {
          modalIsOpen: false
      }
  }
  render() {
    var color = '';
    if(parseInt(this.props.issue.fields.priority.id) === 1 || parseInt(this.props.issue.fields.priority.id) === 2) color = 'red';
    if(parseInt(this.props.issue.fields.priority.id) === 3 || parseInt(this.props.issue.fields.priority.id) === 10100) color = 'orange';
    if(parseInt(this.props.issue.fields.priority.id) === 4) color = 'sky';
    return (
        <div styleName={"issue issue_"+color}>
            <a styleName="link" onClick={this.openModal}>
                <h3 styleName="title">{this.props.issue.fields.summary}</h3>
                <span styleName="state">{this.props.issue.fields.issuetype.name}</span>
                <span styleName="board">{this.props.issue.fields.project.name}</span>
            </a>
            <Modal open={this.state.modalIsOpen} closeModal={this.closeModal} issue={this.props.issue} />
        </div>
    );
  }
  openModal() {
    this.setState({modalIsOpen: true});
  }
  closeModal() {
    this.setState({modalIsOpen: false});
  }
}

export default CSSModules(Issue, styles, {allowMultiple: true});