import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import showdown from 'showdown';
import ReactModal from 'react-modal';

import styles from './modal.css';

const Converter = new showdown.Converter();

class Modal extends Component {
  constructor() {
      super();
      this.outsideClick = this.outsideClick.bind(this);
      this.escDown = this.escDown.bind(this);
  }
  componentDidMount() {
      window.addEventListener("click", this.outsideClick, false);
      window.addEventListener("keydown", this.escDown, false);
  }
  render() {
    return (
        <ReactModal
        isOpen={this.props.open}
        contentLabel="Modal"
        className={{
            base: 'modal',
            afterOpen: 'modal-open',
            beforeClose: 'modal-before-close'
        }}
        overlayClassName={{
            base: 'modal-overlay',
            afterOpen: 'modal-overlay-open',
            beforeClose: 'modal-overlay-before-close'
        }}
        >
            <div className="row modal-header">
                <div className="col-xs-11">
                    <h4 className="modal-title"><img alt="" className="projectImg" height="24" src={this.props.issue.fields.project.avatarUrls["24x24"]} width="24" />{this.props.issue.fields.project.name + "/" + this.props.issue.fields.summary}</h4>
                </div>
                <div className="col-xs-1 text-right">
                    <button onClick={this.props.closeModal} className="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div className="row modal-body">
                <div className="col-xs-12">
                    <p><b>Zadavatel: </b>{this.props.issue.fields.reporter.displayName}</p>
                    <p><b>Status: </b><span styleName={"status status_"+this.props.issue.fields.status.statusCategory.colorName}>{this.props.issue.fields.status.name}</span></p>
                    <p><b>Priorita: </b><img alt="" height="16" src={this.props.issue.fields.priority.iconUrl} width="16"/>{this.props.issue.fields.priority.name }</p>
                    <p><b>Due date: </b>{this.props.issue.fields.duedate}</p>
                    <p><b>Odhad: </b>{parseFloat(this.props.issue.fields.progress.total/3600).toFixed(2)}h</p>
                    <div className="progress">
                        <div className="progress-bar progress-bar-success"></div>
                    </div>
                    {this.props.issue.fields.description && <div dangerouslySetInnerHTML={{__html: Converter.makeHtml(this.props.issue.fields.description)}} />}
                </div>
            </div>
        </ReactModal>
    );
  }
  outsideClick(e) {
    if(this.props.open && e.target.classList.contains('modal-overlay-open')) this.props.closeModal();
  }
  escDown(e) {
    if(e.keyCode === 27 && this.props.open) this.props.closeModal();
  }
}

export default CSSModules(Modal, styles, {allowMultiple: true});