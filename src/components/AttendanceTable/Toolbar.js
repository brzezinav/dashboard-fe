import React, { Component } from 'react';

class Toolbar extends Component {
  render() {
      return (
          <div className="row margin-bottom">
            <div className="col-xs padding-top--thin">
            <h3 className="no-margin">{this.props.label}</h3>
            </div>
            <div className="col-xs text-right">
              <button className="btn btn-orange margin-right" onClick={this.props.goToPrev}><span className="fa fa-arrow-left" /></button>
              <button className="btn btn-orange"><span className="fa fa-arrow-right" onClick={this.props.goToNext} /></button>
            </div>
          </div>
      )
  }
}

export default Toolbar;