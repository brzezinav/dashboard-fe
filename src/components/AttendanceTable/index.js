import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as attendance from '../../actions/attendanceActions';
import moment from 'moment';
import styles from './attendanceTable.css';
import Toolbar from './Toolbar';

moment.locale('cs', {
    weekdays : ['Neděle', 'Pondělí', 'Úterý', 'Sttředa', 'Čtvrtek', 'Pátek', 'Sobota'],
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});

class AttendanceTable extends Component {
    constructor() {
        super();

        this.state = {
            week: new Date()
        }

        this.renderWeek = this.renderWeek.bind(this);
        this.getWeek = this.getWeek.bind(this);
        this.getAttendance = this.getAttendance.bind(this);
        this.getEvents = this.getEvents.bind(this);
        this.goToNext = this.goToNext.bind(this);
        this.goToPrev = this.goToPrev.bind(this);
    }
    render () {
        return (
            <div>
            <div styleName="toolbar">
                <Toolbar label={moment(this.state.week).startOf('week').format('dddd D. M.') + " - " + moment(this.state.week).endOf('week').format('dddd D. M.')} goToNext={this.goToNext} goToPrev={this.goToPrev}  />
            </div>
            <div styleName="table">
                <div className="row" styleName="row header">
                    <div className="col-xs-3 text-left" styleName="cell">
                        Zaměstnanci
                    </div>
                    <div className="col-xs-9">
                        <div className="row">
                            {this.renderWeek()}
                        </div>
                    </div>
                </div>
                {this.getAttendance()}
            </div>
            </div>
        );
    }
    renderWeek() {
        return this.getWeek().map((day, index) => {
                return (
                    <div className="col-xs" styleName="cell" key={index}>
                        {day.format('dddd D. M.')}
                    </div>
                );
            })
    }
    getWeek() {
        var days = [];
        for(var i = 0; i < 7; i++) {
            let day = moment(this.state.week).startOf('week').add(i, 'days').startOf('day');
            days.push(day);
        }
        return days;
    }
    getAttendance() {
        return Object.keys(this.props.users).map(key => {
            if(this.props.users[key].time !== "full" && this.props.users[key].time !== null) {
                return (
                    <div className="row" styleName="row" key={key}>
                        <div className="col-xs-3" styleName="cell user">
                            <span styleName="name">{this.props.users[key].displayName !== undefined ? this.props.users[key].displayName : this.props.users[key].email}</span>{this.props.users[key].position !== null && <span className="label label-orange">{this.props.users[key].position}</span>}
                        </div>
                        <div className="col-xs-9">
                            <div className="row" styleName="row-content">
                                {this.getEvents(key).map((event, index) => {
                                    return (
                                        <div className="col-xs" styleName="cell" key={index}>
                                            {event !== null && (event.unavailable === 1 && "Nedostupný")}
                                            {event !== null && (event.unavailable !== 1 && moment(event.start).format('H:mm') + " - " + moment(event.end).format('H:mm'))}
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                )
            }
        });
    }
    getEvents(uid) {
        let events = [];
        let attendance = [];
        Object.keys(this.props.attendance).map(key => {
            if(this.props.attendance[key].uid === uid) attendance.push(this.props.attendance[key]);
        })

        for(var i = 0; i < 7; i++) {
            var day = moment(this.state.week).startOf('week').add(i, 'days').startOf('day');
            var event = null;
            for(let i = 0; i < attendance.length; i++) {
                if(moment(attendance[i].start).startOf('day').format() === day.format()) event = attendance[i];
            }
            events.push(event);
        }
        
        return events;
    }
    goToNext() {
        this.setState({
            week: new Date(moment(this.state.week).add(1, 'weeks').format())
        });
    }
    goToPrev() {
        this.setState({
            week: new Date(moment(this.state.week).subtract(1, 'weeks').format())
        });
    }
}

function mapStateToProps(store) {
  return { users: store.user.users, attendance: store.attendance.data }
}
function mapDispatchToProps(dispatch) {
  return { attendance_functions: bindActionCreators(attendance, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(AttendanceTable, styles, {allowMultiple: true}));