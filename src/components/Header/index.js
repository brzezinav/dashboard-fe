import React, { Component } from 'react';
import {NavLink as Link} from 'react-router-dom';
import CSSModules from 'react-css-modules';
import {isDescendant} from '../../helpers';

import styles from './header.css';

class Header extends Component {

  componentDidMount() {
    /* When the user clicks on the button, 
    toggle between hiding and showing the dropdown content */
    var dropdowns = document.querySelectorAll(".dropdown-toggle");
    for (var i = 0; i < dropdowns.length; i++) {
      dropdowns[i].addEventListener("click", (e) => {
        var dropdown;
        for(var i = 0; i < e.path.length; i++) {
          if(e.path[i].classList !== undefined && e.path[i].classList.contains('dropdown-toggle')) dropdown = e.path[i].nextSibling;
        }
        if(dropdown.classList.contains('active')) {
          dropdown.classList.remove("active");
        }
        else {
          dropdown.classList.add("active");
        }
      });
    }
    window.addEventListener("click", (e) => {
      var dropdowns = document.querySelectorAll(".dropdown-toggle");
      
      for (var i = 0; i < dropdowns.length; i++) {
        if(!isDescendant(dropdowns[i], e.target) && document.querySelectorAll(".dropdown-menu.active")[0] !== undefined) {
          document.querySelectorAll(".dropdown-menu.active")[0].classList.remove("active");
        }
      }
    }, false);

  }

  render() {
    return (
        <div styleName="header">
          <div className="container-fluid">
            <div styleName="header-brand header-left">
              <Link to="/dashboard">Evolve dashboard</Link>
            </div>
            <ul styleName="nav header-left">
            </ul>
            <ul styleName="nav header-right">
              <li styleName="dropdown">
                <a href="#" className="dropdown-toggle"><span className="fa fa-user"></span><span styleName="user">{this.props.user.email}</span></a>
                <ul className="dropdown-menu" styleName="dropdown-menu">
                  <li><Link to="/account">Můj účet</Link></li>
                  <li><a onClick={() => this.props.logout()}>Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
    );
  }
}

export default CSSModules(Header, styles, {allowMultiple: true});