import React, { Component } from 'react';
import CSSModules from 'react-css-modules';

import styles from './loader.css';

class Loader extends Component {

  render() {
    return (
        <div styleName="loader" className="wrapper">
            <div styleName="spinner">
                <div styleName="bounce1"></div>
                <div styleName="bounce2"></div>
                <div></div>
            </div>
        </div>
    );
  }
}

export default CSSModules(Loader, styles, {allowMultiple: true});