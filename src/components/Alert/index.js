import React, {Component} from 'react';
import CSSModules from 'react-css-modules';

import styles from './alert.css';

class Alert extends Component {
    constructor() {
        super();

        this.close = this.close.bind(this);
        this.state = {
            visible: true
        }
    }
    componentDidMount() {
        setTimeout(this.close, 5000);
    }
    componentWillUnmount() {
        this.close();
    }
    render () {
        if(this.props.children !== "")
        return (
            <div styleName={"alert alert-"+this.props.type} className={!this.state.visible && "hidden"}>
                <button type="button" className="close" styleName="close" data-dismiss="alert" aria-label="Close" onClick={this.close}><span aria-hidden="true">×</span></button>
                {this.props.children}
            </div>
        );
    }
    close() {
        this.setState({
            visible: false
        });
        this.props.onClose();
    }
}

export default CSSModules(Alert, styles, {allowMultiple: true});