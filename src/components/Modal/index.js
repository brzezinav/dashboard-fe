import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import showdown from 'showdown';
import ReactModal from 'react-modal';

import styles from './modal.css';


class Modal extends Component {
  constructor() {
      super();
      this.outsideClick = this.outsideClick.bind(this);
      this.escDown = this.escDown.bind(this);
  }
  componentDidMount() {
      window.addEventListener("click", this.outsideClick, false);
      window.addEventListener("keydown", this.escDown, false);
  }
  render() {
    return (
        <ReactModal
        isOpen={this.props.open}
        contentLabel="Modal"
        className={{
            base: 'modal',
            afterOpen: 'modal-open',
            beforeClose: 'modal-before-close'
        }}
        overlayClassName={{
            base: 'modal-overlay',
            afterOpen: 'modal-overlay-open',
            beforeClose: 'modal-overlay-before-close'
        }}
        >
            <div className="row modal-header">
                <div className="col-xs-11">
                    <h3 className="modal-title">{this.props.title}</h3>
                </div>
                <div className="col-xs-1 text-right">
                    <button onClick={this.props.closeModal} className="close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div className="row modal-body">
                <div className="col-xs-12">
                    {this.props.body}
                </div>
            </div>
        </ReactModal>
    );
  }
  outsideClick(e) {
    if(this.props.open && e.target.classList.contains('modal-overlay-open')) this.props.closeModal();
  }
  escDown(e) {
    if(e.keyCode === 27 && this.props.open) this.props.closeModal();
  }
}

export default CSSModules(Modal, styles, {allowMultiple: true});