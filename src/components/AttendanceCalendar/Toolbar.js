import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as attendance from '../../actions/attendanceActions';

class Toolbar extends Component {
  constructor() {
      super();

      this.goToNext = this.goToNext.bind(this);
      this.goToPrev = this.goToPrev.bind(this);
      this.createAttendance = this.createAttendance.bind(this);
  }  
  render() {
      return (
          <div className="row margin-bottom">
            <div className="col-xs">
              <button className="btn btn-orange margin-right" onClick={() => this.goToPrev()}><span className="fa fa-arrow-left" /></button>
              <button className="btn btn-orange"><span className="fa fa-arrow-right" onClick={() => this.goToNext()} /></button>
            </div>
            <div className="col-xs text-center padding-top--thin">
                <strong>{this.props.label}</strong>
            </div>
            <div className="col-xs text-right">
              <button className="btn btn-orange" onClick={this.createAttendance}><span className="fa fa-plus margin-right" />Přidat přítomnost</button>
            </div>
          </div>
      )
  }
  createAttendance() {
    this.props.attendance_functions.toggleModal();
  }
  goToNext() {
    this.props.date.setMonth(this.props.date.getMonth() + 1);
    this.props.onNavigate('next');
  }
  goToPrev() {
    this.props.date.setMonth(this.props.date.getMonth() - 1);
    this.props.onNavigate('prev');
  }
}

function mapStateToProps(store) {
  return {attendance: store.attendance }
}
function mapDispatchToProps(dispatch) {
  return {attendance_functions: bindActionCreators(attendance, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);