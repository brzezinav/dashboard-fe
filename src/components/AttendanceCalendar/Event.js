import React from 'react';
import moment from 'moment';

const Event = ({event}) => {
    return (
        <div>
            {event.description}<br/>
            {event.unavailable !== 1 && moment(event.start).format('H:mm') + " - " + moment(event.end).format('H:mm')}
        </div>
    )
}

export default Event;