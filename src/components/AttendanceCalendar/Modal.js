import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TimePicker from 'rc-time-picker';
import DayPicker, { DateUtils } from 'react-day-picker';
import Modal from '../../components/Modal';
import moment from 'moment';
import * as attendance from '../../actions/attendanceActions';

class attendanceModal  extends Component {
    constructor(props) {
        super(props);
        this.addAttendance = this.addAttendance.bind(this);
        this.removeAttendance = this.removeAttendance.bind(this);
        this.combineDateTime = this.combineDateTime.bind(this);
        this.checkAvaibility = this.checkAvaibility.bind(this);
        
        this.state = {
            unavailable: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.attendance.calendar.edit.unavailable !== undefined) {
            this.setState({
                unavailable: nextProps.attendance.calendar.edit.unavailable === 1 ? true : false
            });
        }
    }
    render() {
        return (
            <Modal title={Object.keys(this.props.attendance.calendar.edit).length === 0 ? "Přidat přítomnost" : "Upravit záznam"} body={this.editModal()} open={this.props.open} closeModal={this.props.closeModal} />
        );
    }
    editModal() {
        return (
            <form>
                <div className="checkbox">
                    <input type="checkbox" id="unavailable" name="unavailable" onChange={this.checkAvaibility}  defaultChecked={this.state.unavailable} />
                    <label htmlFor="unavailable">{this.props.user.gender === 1 ? "Nedostupná": "Nedostupný"}</label>
                </div>
                <div className="form-group">
                    <label>Popis:</label>
                    <input type="text" className="form-control" ref={(input) => { this.description = input; }} defaultValue={Object.keys(this.props.attendance.calendar.edit).length !== 0 ? this.props.attendance.calendar.edit.description : ""} />
                </div>
                <div className="form-inline">
                    <div className="form-group">
                        <label>Start:</label>
                        <TimePicker disabled={this.state.unavailable} defaultValue={this.props.attendance.calendar.interval.start} showSecond={false} onChange={(value) => this.props.attendance_functions.setInterval("start", value)} />
                    </div>
                    <div className="form-group">
                        <label>Konec:</label>
                        <TimePicker disabled={this.state.unavailable} defaultValue={this.props.attendance.calendar.interval.end} showSecond={false} onChange={(value) => this.props.attendance_functions.setInterval("end", value)} />
                    </div>
                </div>
                <div className="form-group">
                    <label>Opakovat:</label>
                    <DayPicker selectedDays={this.props.attendance.calendar.selectedDays} onDayClick={this.handleDayClick} initialMonth={this.props.attendance.calendar.selectedDays.length ? new Date(this.props.attendance.calendar.selectedDays[0]) : new Date()} />
                </div>
                {Object.keys(this.props.attendance.calendar.edit).length !== 0 ? (
                    <div className="row">
                        <div className="col-xs">
                            <button className="btn btn-orange" onClick={this.addAttendance}><span className="fa fa-pencil-square-o margin-right" />Upravit záznam</button>
                        </div>
                        <div className="col-xs text-right">
                            <button className="btn btn-orange" onClick={this.removeAttendance}><span className="fa fa-trash-o margin-right" />Odstranit</button>
                        </div>
                    </div>
                ) : (
                    <div className="row">
                        <div className="col-xs">
                            <button className="btn btn-orange" onClick={this.addAttendance}><span className="fa fa-plus margin-right" />Přidat přítomnost</button>
                        </div>
                    </div>
                )}
            </form>
        );
    }
    addAttendance() {
        if(Object.keys(this.props.attendance.calendar.edit).length !== 0) {
            this.props.attendance_functions.editAttendance(this.props.attendance.calendar.edit.id, {
                start: this.combineDateTime(new Date(this.props.attendance.calendar.selectedDays[0]), new Date(this.props.attendance.calendar.interval.start)),
                end: this.combineDateTime(new Date(this.props.attendance.calendar.selectedDays[0]), new Date(this.props.attendance.calendar.interval.end)),
                description: this.description.value,
                uid: this.props.user.uid,
                unavailable: Number(this.state.unavailable)
            });
        }
        else {
            for(let i = 0; i < this.props.attendance.calendar.selectedDays.length; i++) {
                this.props.attendance_functions.addAttendance({
                    start: this.combineDateTime(this.props.attendance.calendar.selectedDays[i], this.props.attendance.calendar.interval.start),
                    end: this.combineDateTime(this.props.attendance.calendar.selectedDays[i], this.props.attendance.calendar.interval.end),
                    description: this.description.value,
                    uid: this.props.user.uid,
                    unavailable: Number(this.state.unavailable)
                });
            }
        }
        this.props.closeModal();
    }
    removeAttendance() {
        this.props.attendance_functions.removeAttendance(this.props.attendance.calendar.edit.id);
        this.props.closeModal();
    }
    combineDateTime(date, time) {
        return moment().set({'year': moment(date).years(), 'month': moment(date).month(), 'date': moment(date).date(), 'hour': moment(time).hour(), 'minute': moment(time).minute(), 'second': 0}).format("YYYY-MM-DD HH:mm:ss");
    }
    handleDayClick = (day, { selected }) => {
        if(Object.keys(this.props.attendance.calendar.edit).length !== 0) {
            this.props.attendance_functions.selectDays([day]);
        }
        else {
            const { selectedDays } = this.props.attendance.calendar;
            if (selected) {
            const selectedIndex = selectedDays.findIndex(selectedDay =>
                DateUtils.isSameDay(selectedDay, day)
            );
            selectedDays.splice(selectedIndex, 1);
            } else {
            selectedDays.push(day);
            }
            this.props.attendance_functions.selectDays(selectedDays);
        }
    };
    checkAvaibility() {
        this.setState({
            unavailable: !this.state.unavailable
        });
    }
}

function mapStateToProps(store) {
  return { user: store.user.user, attendance: store.attendance }
}
function mapDispatchToProps(dispatch) {
  return {attendance_functions: bindActionCreators(attendance, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(attendanceModal);