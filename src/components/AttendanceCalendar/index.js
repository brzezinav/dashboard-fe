import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as user from '../../actions/userActions';
import * as issues from '../../actions/issueActions';
import * as attendance from '../../actions/attendanceActions';
import BigCalendar from 'react-big-calendar';
import Modal from './Modal';
import Toolbar from './Toolbar';
import moment from 'moment';
import styles from './calendar.css';
import Event from './Event';

BigCalendar.momentLocalizer(moment);


moment.locale('cs', {
    weekdaysShort : ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    },
    months: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec']
});

class AttendanceCalendar extends Component {
  constructor() {
    super();
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      date: new Date()
    }
  }
  componentWillMount() {
    this.setState({
      date: new Date()
    });
  }
  render() {
      return (
        <div styleName="calendar-wrapper">
          <div className="row">
            <div className="col-xs-12" styleName="calendar">
              <BigCalendar
                defaultDate={this.state.date}
                events={this.props.events}
                eventPropGetter={(event, start, end, isSelected) => {
                  return { className: event.unavailable && 'unavailable' }
                }}
                startAccessor='start'
                endAccessor='end'
                titleAccessor='user'
                defaultView='month'
                selectable
                onSelectEvent={event => this.toggleModal([new Date([event.start])], event)}
                onSelectSlot={(slotInfo) => this.toggleModal(slotInfo.slots)}
                components={{
                  event: Event,
                  toolbar: Toolbar
                }}
              />
            </div>
          </div>
          <Modal title="Přidat přítomnost" open={this.props.attendance.modal} closeModal={this.toggleModal} />
        </div>
      )
  }
  toggleModal(slots = [], edit = {}) {
    this.props.attendance_functions.toggleModal(slots, edit);
  }
}

function mapStateToProps(store) {
  return { user: store.user.user, issues: store.issues, attendance: store.attendance }
}
function mapDispatchToProps(dispatch) {
  return { issue_functions: bindActionCreators(issues, dispatch), user_functions: bindActionCreators(user, dispatch), attendance_functions: bindActionCreators(attendance, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(AttendanceCalendar, styles));