import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import {Redirect} from 'react-router-dom';
import Modal from '../Modal';
import { Scrollbars } from 'react-custom-scrollbars';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as user from '../../actions/userActions';

import styles from './app.css';

import Loader from '../Loader/';
import Alert from '../Alert/';
import Header from '../Header/';
import Menu from '../Menu/';
import Footer from '../Footer/';

const teams = [
  'Panda',
  'Java'
];

const nav = [
  {
    path: '/dashboard/',
    name: 'Můj přehled'
  },
  {
    path: '/tasks/',
    name: 'Úkoly'
  },
  {
    path: '/attendance/',
    name: 'Přítomnost'
  },
  {
    path: '/projects/',
    name: 'Projekty'
  }
];

class App extends Component {

  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
    this.welcomeModal = this.welcomeModal.bind(this);

    this.state = {
      modal_open: this.props.user.first_login == 1 ? true : false
    }
  }

  componentWillMount() {
    this.props.user_functions.setUser();
  }

  render() {
    if(this.props.user.logged !== undefined) {
      if (!this.props.user.logged) {
        return (
          <Redirect to="/"/>
        )
      }
      else {
      return (
      <div styleName="app" className="wrapper">
      <Scrollbars style={{ width: '100vw', height: '100vh' }}>
            <div className="row">
              <div className="col-xs-12">
                  <Header logout={this.props.user_functions.logout} user={this.props.user} />
              </div>
            </div>  
            <div className="wrapper" styleName="content">
                <div className="col-md-2 no-padding-right">
                    <Menu nav={nav} />
                </div>
                {this.props.children}
            </div>
            <div className="row">
              <Footer />
            </div>
            <Modal title={this.props.user.first_login && "Vítej v naší aplikaci"} body={this.props.user.first_login && this.welcomeModal()} open={this.state.modal_open} closeModal={this.closeModal} />
            </Scrollbars>
      </div>
      )
      }
    }
    else {
      return(
        <Loader/>
      )
    }
  }
  closeModal() {
    this.setState({
      ...this.state,
      modal_open: false
    });
    this.props.user_functions.firstLogin(this.props.user.uid);
  }
  welcomeModal() {
    return (
      <div>
        Ahoj, vítej v naší aplikaci! Začni prosím nastavením typu svého úvazku a pozice.
        <h4>Co to umí?</h4>
        <ul className="feature-list">
          <li>Přehled issues</li>
          <li>Evidence přítomnosti ve firmě pro naše part-time kolegy (náhrada za wheniwork)</li>
          <li>Tabulka s přehledem projektů které maintainujeme</li>
          <li>Jedná se o real-time aplikaci (pokud se cokoliv změní, ať už v Jiřině, přítomnosti, nebo tabulce projektů, změnu ihned uvidíte)</li>
        </ul>
        <h4>Co se chystá?</h4>
        <ul>
          <li>Řazení vlastních issues</li>
          <li>Změna stavu issues</li>
          <li>Změna assignee</li>
          <li>Přihlášení přes google a synchronizace s dalšími službami (snad někdy v budoucnu)</li>
          <li>Spousta bug a performance fixů</li>
        </ul>
        <p>Jakékoliv nápady a náměty na vylepšení, či rozšíření aplikce neváhejte posílat na <a href="mailto:vaclav.brzezina@etnetera.cz">vaclav.brzezina@etnetera.cz</a></p>
      </div>
    )
  }
}

function mapStateToProps(store) {
  return { user: store.user.user}
}
function mapDispatchToProps(dispatch) {
  return {user_functions: bindActionCreators(user, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CSSModules(App, styles));